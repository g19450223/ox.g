import java.util.Scanner;
public class gameox{
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static int row, column;
    static char player = 'X';
    static String game = "start";
    static String winner = "?";

    public static void showtable(){
        for(int i = 0;i<3;i++){
           for(int j = 0;j<3;j++){
            System.out.print(table[i][j]+" ");
           }
           System.out.println();
        }
    }
    public static void inputrowcol(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Turn "+player+" please input Row and Column(0-2): ");
        row = kb.nextInt();
        column = kb.nextInt();
        System.out.println(row+" "+column); 
        table[row][column] = player;
    }
    public static void switchplayer(){
        if(player=='X'){player = 'O';}
        else{player='X';}
    }
    public static void checkwin(){
        if(table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X'){
            game = "end"; winner = "X";
        }
        else if(table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O'){
            game = "end"; winner = "O";
        }
        for(int i=0;i<3;i++){ /* เช็คแนวนอน */ 
            for(int j=0;j<1;j++){
                if(table[i][j] == 'X' && table[i][j+1] == 'X' && table[i][j+2] == 'X'){
                    game = "end"; winner = "X";
                }
                else if(table[i][j] == 'O' && table[i][j+1] == 'O' && table[i][j+2] == 'O'){
                    game = "end"; winner = "O";
                }
            }
        }
        for(int i=0;i<1;i++){ /* แนวนอน */
            for(int j=0;j<3;j++){
                if(table[i][j] == 'X' && table[i+1][j] == 'X' && table[i+2][j] == 'X'){
                    game = "end"; winner = "X";
                }
                else if(table[i][j] == 'O' && table[i+1][j] == 'O' && table[i+2][j] == 'O'){
                    game = "end"; winner = "O";
                }
            }
        }

    }
    public static void main(String[] args) {
        while(game=="start"){
            showtable();
            inputrowcol();
            checkwin();
            switchplayer();
        }
        showtable();
        System.out.println("The Winner is "+winner);
    }
}
